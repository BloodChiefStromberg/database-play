package database_play;


import javax.persistence.*;

@Entity
@Table(schema = "aaron") // necessary per https://stackoverflow.com/questions/5714443/ora-00942-sqlexception-with-hibernate-unable-to-find-a-table
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String firstName; // I should really change the table but for now
    private String lastName;
    private String email;

//    public Employee(long id, String firstName, String lastName, String email) {
//        this.id = id;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.email = email;
//    }


//    public Employee() {
//        // TODO bad initializing
//        this.id = 0;
//        firstName = "UNSET";
//        lastName = "UNSET";
//        email = "UNSET";
//    }

    @Override
    public String toString() {
        return String.format(
                "Employee[id=%d, firstName='%s', lastName='%s']",
                id, firstName, lastName);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
