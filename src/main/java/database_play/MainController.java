package database_play;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path="/employees")
public class MainController {
    // TODO start up DB

    @Autowired
    private EmployeeRepository employeeRepository;

    // should really be post
    @GetMapping(path="/add") // @responsebody means that we can just return an object instead of a view!
    public @ResponseBody String addNewEmployee(@RequestParam String name, @RequestParam String email) {
        Employee emp = new Employee();
        emp.setFirstName(name);
        emp.setEmail(email);
        employeeRepository.save(emp);
        return "Saved";
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }
}
