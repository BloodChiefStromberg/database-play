package database_play;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DatabaseLoader implements CommandLineRunner {


    private static final Logger log = LoggerFactory.getLogger(DatabaseLoader.class);

    @Autowired // this finds the template the springboot makes for us (because we're using spring-jdbc, which produces it?)
    JdbcTemplate jdbcTemplate;

    @Override
    public void run(String... strings) throws Exception {

        // Split up the array of whole names into an array of first/last names
        List<Object[]> splitUpNames = Stream.of("John Woo", "Jeff Dean", "Josh Bloch", "Josh Long")
                .map(name -> name.split(" "))
                .collect(Collectors.toList());

        // Use a Java 8 stream to print out each tuple of the list
        splitUpNames.forEach(name -> log.info(String.format("Inserting employee record for %s %s", name[0], name[1])));

        // Uses JdbcTemplate's batchUpdate operation to bulk load data
        // TODO how can i get a repository to actually do this for me (see notes on #3)
        jdbcTemplate.batchUpdate("INSERT INTO employees(first_name, last_name) VALUES (?,?)", splitUpNames);

    }
}
