package database_play;



import oracle.jdbc.pool.OracleDataSource;

import java.sql.ResultSet;

// This file connects to oracle with us managing the JDBC connection ourselves. We can use spring to make it way easier
public class JavaClient {

    public static void main(String[] args) throws Exception {
        OracleDataSource ds;
        ds = new OracleDataSource();

        DatabaseConnection dbConnection = new DatabaseConnection(ds);

        ResultSet results = dbConnection.getAllEmployees();

        while (results.next()) {
            System.out.println(results.getInt(1) + " " +
                    results.getString(2) + " " +
                    results.getString(3) + " " +
                    results.getString(4));
        }
    }
}
