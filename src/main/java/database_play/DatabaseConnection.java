package database_play;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import oracle.jdbc.pool.OracleDataSource;

// Copied mostly from https://docs.oracle.com/cd/E11882_01/appdev.112/e12137/getconn.htm#TDPJD146
public class DatabaseConnection {

    public DatabaseConnection(OracleDataSource ds) {
        this.ds = ds;
    }

    // TODO not hardcode, not so ugly
    private String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:XE";
    private String userid = "hr";
    private String password = "admin";
    private OracleDataSource ds;
    private Connection conn;

    // for setter injection
    public void setDs(OracleDataSource ds) {
        this.ds = ds;
    }

    public ResultSet getAllEmployees() throws SQLException{
        getDBConnection();

        Statement statement = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_READ_ONLY);

        String query = "SELECT * FROM Employees ORDER BY employee_id";

        System.out.println("\nExecuting query: " + query);
        ResultSet results = statement.executeQuery(query);

        return results;
    }

    public void getDBConnection() throws SQLException{
//        OracleDataSource ds;
//        ds = new OracleDataSource();

        ds.setURL(jdbcUrl);
        conn=ds.getConnection(userid,password);

    }
}