package database_play;

import oracle.jdbc.pool.OracleDataSource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class DBConnectionTest {

    // TODO use spring to inject this easier
    @Mock
    OracleDataSource ds;

    @Test
    public void testConnection() {
        DatabaseConnection dbConnection = new DatabaseConnection(ds);
        dbConnection.setDs(ds);

        try {
            dbConnection.getDBConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            // doesn't work if the arguments are null
            //verify(ds).getConnection(anyString(), anyString()); //do I have to catch here? sinced mocked?
            verify(ds).getConnection(nullable(String.class), nullable(String.class));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
